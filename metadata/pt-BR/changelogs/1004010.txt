1.4.10
  * Imagens traduzidas para F-Droid
1.4.9
  * Algumas pequenas correções de tradução
1.4.8
  * Adicionada tradução em polaco. Graças ao verdulo.
  * Adicionada tradução em esperanto. Graças ao verdulo.
1.4.7
  * O inverno está a chegar. Adicionado alguns níveis novos.
  * Eu fiquei sem ideias. Por favor, envie-me ideias para novos níveis.
